# Sandbox

To install:  
`mvn clean install`    

Then in `project/target` folder:  
`java -jar sandbox-0.0.1-SNAPSHOT.jar`  

Swagger is available:  
`http://localhost:8080/swagger-ui.html`
