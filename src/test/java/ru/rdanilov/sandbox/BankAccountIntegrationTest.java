package ru.rdanilov.sandbox;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import ru.rdanilov.sandbox.model.BankAccount;
import ru.rdanilov.sandbox.repository.BankAccountRepository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author rdanilov
 * @since 06.07.2020
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SandboxApplication.class)
@Transactional
@Slf4j
public class BankAccountIntegrationTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void findAllBankAccountsTest() throws Exception {
        List<BankAccount> bankAccounts = bankAccountRepository.findAll();
        mockMvc.perform(
                get("/bank_account")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(bankAccounts.size())));
    }

    @Test
    public void withdrawTest() throws Exception {
        String billing = "11111";
        BankAccount bankAccount = bankAccountRepository.findBankAccountByBillingId(billing);
        double withdrawAmount = 5000;
        double expectedAmount = bankAccount.getAmount() - withdrawAmount;

        mockMvc.perform(
                put("/bank_account/withdraw/" + billing + "/" + withdrawAmount)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful());

        BankAccount updatedBankAccount = bankAccountRepository.findBankAccountByBillingId(billing);
        assertEquals(expectedAmount, updatedBankAccount.getAmount(), 0.0000001);
    }

    @Test
    public void depositTest() throws Exception {
        String billing = "11111";
        BankAccount bankAccount = bankAccountRepository.findBankAccountByBillingId(billing);
        double withdrawAmount = 5000;
        double expectedAmount = bankAccount.getAmount() + withdrawAmount;

        mockMvc.perform(
                put("/bank_account/deposit/" + billing + "/" + withdrawAmount)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful());

        BankAccount updatedBankAccount = bankAccountRepository.findBankAccountByBillingId(billing);
        assertEquals(expectedAmount, updatedBankAccount.getAmount(), 0.0000001);
    }

    @Test
    public void transferTest() throws Exception {
        String billingFrom = "11111";
        String billingTo = "22222";
        BankAccount bankAccountFrom = bankAccountRepository.findBankAccountByBillingId(billingFrom);
        BankAccount bankAccountTo = bankAccountRepository.findBankAccountByBillingId(billingTo);
        double transferAmount = 5000;
        double expectedAmountFrom = bankAccountFrom.getAmount() - transferAmount;
        double expectedAmountTo = bankAccountTo.getAmount() + transferAmount;

        mockMvc.perform(
                put("/bank_account/transfer/" + billingFrom + "/" + billingTo + "/" + transferAmount)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful());

        BankAccount updatedBankAccountFrom = bankAccountRepository.findBankAccountByBillingId(billingFrom);
        BankAccount updatedBankAccountTo = bankAccountRepository.findBankAccountByBillingId(billingTo);

        assertEquals(expectedAmountFrom, updatedBankAccountFrom.getAmount(), 0.0000001);
        assertEquals(expectedAmountTo, updatedBankAccountTo.getAmount(), 0.0000001);
    }

    @Test
    public void transferAccountNotFoundTest() throws Exception {
        String billingFrom = "11111";
        String billingTo = "55555";
        double transferAmount = 5000;

        mockMvc.perform(
                put("/bank_account/transfer/" + billingFrom + "/" + billingTo + "/" + transferAmount)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is4xxClientError())
                .andExpect(content().string("Bank account with " + billingTo + " billing not found"));
    }

    @Test
    public void transferTheSameBillingIdTest() throws Exception {
        String billingFrom = "11111";
        String billingTo = "11111";
        double transferAmount = 5000;

        mockMvc.perform(
                put("/bank_account/transfer/" + billingFrom + "/" + billingTo + "/" + transferAmount)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is4xxClientError())
                .andExpect(content().string("It's not allowed to use the same billing id"));
    }

    @Test
    public void withdrawWithNegativeBalanceTest() throws Exception {
        String billing = "11111";
        double withdrawAmount = 20000;

        mockMvc.perform(
                put("/bank_account/withdraw/" + billing + "/" + withdrawAmount)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is4xxClientError())
                .andExpect(content().string("Balance of bank account with " + billing + " billing can't be negative"));
    }

    @Test
    public void withdrawDepositMultiThreadTest() throws Exception {
        String billing = "11111";

        BankAccount bankAccount = bankAccountRepository.findBankAccountByBillingId(billing);

        double expectedAmount = bankAccount.getAmount() + 2000;

        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch finishLatch = new CountDownLatch(2);

        Thread depositWorker = new Thread(createBalanceChange(startLatch, finishLatch, "deposit", billing, 15, 200));
        Thread withdrawWorker = new Thread(createBalanceChange(startLatch, finishLatch, "withdraw", billing, 10, 100));

        depositWorker.start();
        withdrawWorker.start();

        startLatch.countDown();
        finishLatch.await();

        entityManager.clear();

        BankAccount updatedBankAccount = bankAccountRepository.findBankAccountByBillingId(billing);

        assertEquals(expectedAmount, updatedBankAccount.getAmount(), 0.0000001);
    }

    @Test
    @Transactional
    public void transferMultiThreadTest() throws Exception {

        String firstBilling = "11111";
        String secondBilling = "22222";
        String thirdBilling = "33333";

        BankAccount firstAccount = bankAccountRepository.findBankAccountByBillingId(firstBilling);
        BankAccount secondAccount = bankAccountRepository.findBankAccountByBillingId(secondBilling);
        BankAccount thirdAccount = bankAccountRepository.findBankAccountByBillingId(thirdBilling);

        double firstExpectedAmount = firstAccount.getAmount() + 2000;
        double secondExpectedAmount = secondAccount.getAmount() - 1000;
        double thirdExpectedAmount = thirdAccount.getAmount() - 1000;

        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch finishLatch = new CountDownLatch(3);

        Thread firstTransfer = new Thread(createTransferRunnable(startLatch, finishLatch,firstBilling, secondBilling, 100, 80));
        Thread secondTransfer = new Thread(createTransferRunnable(startLatch, finishLatch, secondBilling, thirdBilling, 100, 90));
        Thread thirdTransfer = new Thread(createTransferRunnable(startLatch, finishLatch, thirdBilling, firstBilling, 100, 100));

        firstTransfer.start();
        secondTransfer.start();
        thirdTransfer.start();

        startLatch.countDown();
        finishLatch.await();

        entityManager.clear();

        BankAccount firstUpdatedAccount = bankAccountRepository.findBankAccountByBillingId(firstBilling);
        BankAccount secondUpdatedAccount = bankAccountRepository.findBankAccountByBillingId(secondBilling);
        BankAccount thirdUpdatedAccount = bankAccountRepository.findBankAccountByBillingId(thirdBilling);

        assertEquals(firstExpectedAmount, firstUpdatedAccount.getAmount(), 0.0000001);
        assertEquals(secondExpectedAmount, secondUpdatedAccount.getAmount(), 0.0000001);
        assertEquals(thirdExpectedAmount, thirdUpdatedAccount.getAmount(), 0.0000001);
    }

    private Runnable createBalanceChange(CountDownLatch startLatch, CountDownLatch finishLatch, String method, String billing, double amount, int times) {
        return () -> {
            try {
                startLatch.await();
                for (int i = 0; i < times; i++) {
                    mockMvc.perform(
                            put("/bank_account/" + method + "/" + billing + "/" + amount)
                                    .contentType(MediaType.APPLICATION_JSON)
                    ).andExpect(status().is2xxSuccessful());
                }
            } catch (Exception ignored) {}
            finally {
                finishLatch.countDown();
            }
        };
    }

    private Runnable createTransferRunnable(CountDownLatch startLatch, CountDownLatch finishLatch, String from, String to, double amount, int times) {
        return () -> {
            try {
                startLatch.await();
                for (int i = 0; i < times; i++) {
                    mockMvc.perform(
                            put("/bank_account/transfer/" + from + "/" + to + "/" + amount)
                                    .contentType(MediaType.APPLICATION_JSON)
                    ).andExpect(status().is2xxSuccessful());
                }
            } catch (Exception ignored) {}
            finally {
                finishLatch.countDown();
            }
        };
    }
}
