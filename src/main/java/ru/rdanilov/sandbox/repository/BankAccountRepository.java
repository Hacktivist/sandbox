package ru.rdanilov.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rdanilov.sandbox.model.BankAccount;

/**
 * @author rdanilov
 * @since 05.07.2020
 */
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    BankAccount findBankAccountByBillingId(String billingId);
}
