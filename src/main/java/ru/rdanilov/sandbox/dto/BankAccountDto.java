package ru.rdanilov.sandbox.dto;

import lombok.Data;

/**
 * @author rdanilov
 * @since 05.07.2020
 */
@Data
public class BankAccountDto {
    private long id;
    private String billingId;
    private String ownerName;
    private double amount;
}
