package ru.rdanilov.sandbox.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rdanilov.sandbox.dto.BankAccountDto;
import ru.rdanilov.sandbox.exception.BankAccountException;
import ru.rdanilov.sandbox.mapper.BankAccountMapper;
import ru.rdanilov.sandbox.model.BankAccount;
import ru.rdanilov.sandbox.repository.BankAccountRepository;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author rdanilov
 * @since 05.07.2020
 */
@Service
public class BankAccountServiceImpl implements BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankAccountMapper bankAccountMapper;

    private final Map<String, Object> locks = new ConcurrentHashMap<>();

    @Override
    public List<BankAccountDto> getAllAccounts() {
        return bankAccountMapper.modelToDTO(bankAccountRepository.findAll());
    }

    @Override
    public void transfer(String from, String to, double amount) {
        if (from.equals(to)) {
            throw new BankAccountException("It's not allowed to use the same billing id");
        }

        locks.putIfAbsent(from, new Object());
        locks.putIfAbsent(to, new Object());

        String [] billings = new String[] {from, to};
        Arrays.sort(billings);

        synchronized (locks.get(billings[0])) {
            synchronized (locks.get(billings[1])) {
                BankAccount bankAccountFrom = findAccount(from);
                BankAccount bankAccountTo = findAccount(to);
                transferUnderTransaction(bankAccountFrom, bankAccountTo, amount);
            }
        }
    }

    @Transactional
    public void transferUnderTransaction(BankAccount from, BankAccount to, double amount) {
        changeBalance(from, -amount);
        changeBalance(to, amount);
        bankAccountRepository.save(from);
        bankAccountRepository.save(to);
    }

    @Override
    public void changeBalance(String billing, double amount) {
        locks.putIfAbsent(billing, new Object());
        synchronized (locks.get(billing)) {
            BankAccount bankAccount = findAccount(billing);
            changeBalanceUnderTransaction(bankAccount, amount);
        }
    }

    @Transactional
    public void changeBalanceUnderTransaction(BankAccount bankAccount, double amount) {
        changeBalance(bankAccount, amount);
        bankAccountRepository.save(bankAccount);
    }

    private void changeBalance(BankAccount bankAccount, double amount) {
        double resultAmount = BigDecimal.valueOf(bankAccount.getAmount()).add(new BigDecimal(amount)).doubleValue();

        if (resultAmount < 0) {
            throw new BankAccountException("Balance of bank account with " + bankAccount.getBillingId() + " billing can't be negative");
        }

        bankAccount.setAmount(resultAmount);
    }

    private BankAccount findAccount(String billing) {
        BankAccount account = bankAccountRepository.findBankAccountByBillingId(billing);
        if (account != null) {
            return account;
        }
        throw new BankAccountException("Bank account with " + billing + " billing not found");
    }
}
