package ru.rdanilov.sandbox.service;

import ru.rdanilov.sandbox.dto.BankAccountDto;

import java.util.List;

/**
 * @author rdanilov
 * @since 05.07.2020
 */
public interface BankAccountService {

    List<BankAccountDto> getAllAccounts();
    void transfer(String from, String to, double amount);
    void changeBalance(String billing, double amount);
}
