package ru.rdanilov.sandbox.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import ru.rdanilov.sandbox.dto.BankAccountDto;
import ru.rdanilov.sandbox.model.BankAccount;

import java.util.List;

/**
 * @author rdanilov
 * @since 05.07.2020
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BankAccountMapper {

    BankAccountDto modelToDTO(BankAccount bankAccount);

    @IterableMapping(elementTargetType = BankAccountDto.class)
    List<BankAccountDto> modelToDTO(List<BankAccount> bankAccounts);
}
