package ru.rdanilov.sandbox.exception;

/**
 * @author rdanilov
 * @since 05.07.2020
 */
public class BankAccountException extends RuntimeException {

    public BankAccountException(String message) {
        super(message);
    }
}
