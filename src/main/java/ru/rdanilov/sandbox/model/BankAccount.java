package ru.rdanilov.sandbox.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author rdanilov
 * @since 05.07.2020
 */
@Data
@Entity
@Table(name = "bank_account")
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "billing_id")
    private String billingId;

    @Column(name = "owner_name")
    private String ownerName;

    @Column(name = "amount")
    private Double amount;
}
