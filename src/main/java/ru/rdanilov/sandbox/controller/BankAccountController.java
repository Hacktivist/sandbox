package ru.rdanilov.sandbox.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.rdanilov.sandbox.dto.BankAccountDto;
import ru.rdanilov.sandbox.service.BankAccountService;

import java.util.List;

/**
 * @author rdanilov
 * @since 05.07.2020
 */
@RestController
@RequestMapping(value = "/bank_account")
public class BankAccountController {

    @Autowired
    private BankAccountService bankAccountService;

    @GetMapping
    @ResponseBody
    public List<BankAccountDto> getAllAccounts() {
        return bankAccountService.getAllAccounts();
    }

    @PutMapping(value = "/transfer/{from}/{to}/{amount}")
    public void transfer(@PathVariable("from") String billingFrom,
                         @PathVariable("to") String billingTo,
                         @PathVariable("amount") double amount) {
        bankAccountService.transfer(billingFrom, billingTo, amount);
    }

    @PutMapping(value = "/deposit/{billing}/{amount}")
    public void deposit(@PathVariable("billing") String billing, @PathVariable("amount") double amount) {
        bankAccountService.changeBalance(billing, amount);
    }

    @PutMapping(value = "/withdraw/{billing}/{amount}")
    public void withdraw(@PathVariable("billing") String billing, @PathVariable("amount") double amount) {
        bankAccountService.changeBalance(billing, -amount);
    }

}
